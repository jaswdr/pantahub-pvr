//
// Copyright 2018  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
package main

import (
	"log"
	"net/http"
	"os"

	"encoding/json"

	"github.com/go-resty/resty"
	"github.com/gorilla/mux"
	"gitlab.com/pantacor/pantahub-base/devices"
	pvrapi "gitlab.com/pantacor/pvr/api"
)

var apiHost string

func init() {
	apiHost = os.Getenv("PANTAHUB_API_HOST")
	if apiHost == "" {
		apiHost = "https://api.pantahub.com"
	}
}

func getUserDevicePvrRemote(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	usernick := vars["usernick"]
	devicenick := vars["devicenick"]
	step := vars["step"]

	deviceInfo, err := resty.SetDebug(true).R().SetHeader("Authorization", r.Header.Get("Authorization")).
		SetHeader("X-Forwarded-For", r.RemoteAddr).
		SetHeader("X-Forwarded-Proto", r.Proto).
		Get(apiHost + "/devices/np/" + usernick + "/" + devicenick)

	if deviceInfo != nil && deviceInfo.StatusCode() != 200 {
		for k, v := range deviceInfo.Header() {
			for _, v1 := range v {
				w.Header().Add(k, v1)
			}
		}
		w.WriteHeader(deviceInfo.StatusCode())
		w.Write(deviceInfo.Body())
		return
	}

	if err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}

	var remoteInfo pvrapi.PvrRemote
	var device devices.Device
	var pvrRemoteURL string

	err = json.Unmarshal(deviceInfo.Body(), &device)

	if err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
	}

	if step == "" {
		pvrRemoteURL = apiHost + "/trails/" + device.Id.Hex() + "/.pvrremote"
	} else {
		pvrRemoteURL = apiHost + "/trails/" + device.Id.Hex() +
			"/steps/" + step + "/.pvrremote"
	}

	pvrRemoteInfo, err := resty.R().
		SetHeader("Authorization", r.Header.Get("Authorization")).
		SetHeader("X-Forwarded-For", r.RemoteAddr).
		SetHeader("X-Forwarded-Proto", r.Proto).
		Get(pvrRemoteURL)

	if pvrRemoteInfo != nil && pvrRemoteInfo.StatusCode() != 200 {
		w.WriteHeader(pvrRemoteInfo.StatusCode())
		for k, v := range pvrRemoteInfo.Header() {
			for _, v1 := range v {
				w.Header().Add(k, v1)
			}
		}
		w.Write(pvrRemoteInfo.Body())
		return
	}

	if err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(pvrRemoteInfo.Body(), &remoteInfo)

	if err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
	}

	_, err = w.Write(pvrRemoteInfo.Body())

	if err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
	}

	return
}

func main() {

	log.Println("Starting pantahub-pvr service.")
	log.Println("Use PANTAHUB_API_HOST to configure your backend. [default: https://api.pantahub.com]")

	r := mux.NewRouter()
	r.HandleFunc("/{usernick}/{devicenick}/.pvrremote", getUserDevicePvrRemote).Methods("GET")
	r.HandleFunc("/{usernick}/{devicenick}/{step}/.pvrremote", getUserDevicePvrRemote).Methods("GET")

	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(":12367", r))
}
